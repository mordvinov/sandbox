
/* ********** Demo App instance ********** */
var demoApp = angular.module('demoApp', ['ngRoute', 'ui.bootstrap']);

/* ********** Demo App configuration ********** */
demoApp.constant("AppConfig", {
    title: "Demo Application",
    securityCode: 1234
});

/* ********** Routes ********** */
demoApp.config(function ($locationProvider, $routeProvider) {
    $locationProvider.hashPrefix("");

    // Components is the best way for route templates
    // https://code.angularjs.org/1.5.8/docs/guide/component
    $routeProvider
        .when('/personal', { template: '<personal></personal>', activetab: "personal" })
        .when('/email', { template: '<email></email>', activetab: "email" })
        .when('/phone', { template: '<phone></phone>', activetab: "phone" })
        .otherwise('/personal');
});

/* ********** Base controller for index.html ********** */
demoApp.controller("AppController", ["$route", "AppConfig",
    function ($route, config) {
        var $ctrl = this;

        $ctrl.$route = $route; // Needs for detecting current route ($route.current), used for mark nav items as active
        $ctrl.title = config.title;

        $ctrl.links = [
            { id: "personal", title: "Personal", href: "#/personal" },
            { id: "email", title: "Email", href: "#/email" },
            { id: "phone", title: "Phone", href: "#/phone" }
        ];
    }]);

/////////////////////////// T O O L S ///////////////////////////

/* ********** Service for storing shared data between components ********** */
demoApp.service("DataStorage", ["$q", "$uibModal", "AppConfig",
    function ($q, $uibModal, config) {
        this.firstName = undefined;
        this.lastName = undefined;
        this.email = undefined;
        this.phone = undefined;

        // Opens the modal with security code input field
        this.checkSecurityCode = function () {
            var phone = this.phone;

            var deferred = $q.defer(); // angularjs Promise wrapper https://docs.angularjs.org/api/ng/service/$q

            // Wait for closing modal and resolve promise
            $uibModal.open({ controller: "ConfirmSecurityCode", controllerAs: "$ctrl", templateUrl: "confirm-security-code.html" })
                .result.then(function (code) {
                    if (code === config.securityCode) {
                        deferred.resolve(true);
                    }
                    else {
                        alert("Wrong security code!");
                        deferred.reject(false);
                    }
                });

            return deferred.promise;
        };

        // Prevents data losing by showing confirm dialog if data changed
        this.preventRouteChangeIfDataChanged = function (scope, hasChanges) {
            scope.$on('$locationChangeStart', function (event) {
                if (hasChanges() && !confirm("Yout have unsaved changes. Do you want to leave this page?")) {
                    event.preventDefault();
                }
            });
        };
    }]);

/* ********** Controller for security code confirmation modal ********** */
// It couldn't be a component in cause of $uibModalInstance injection issue
demoApp.controller("ConfirmSecurityCode", ["$uibModalInstance", "AppConfig",
    function ($uibModalInstance, config) {
        var $ctrl = this;

        $ctrl.correctCode = config.securityCode;

        $ctrl.title = "Confirm changes";
        $ctrl.code = undefined;

        $ctrl.ok = function () {
            $uibModalInstance.close(+$ctrl.code); // input binding passes the code value as string, converts it to number
        }

        $ctrl.cancel = function () {
            $uibModalInstance.dismiss("cancel");
        }
    }]);
/////////////////////////// E N D   T O O L S ///////////////////////////    


/////////////////////////// C O M P O N E N T S ///////////////////////////
/**
 * A Component is a special kind of directive that uses a simpler configuration which is suitable for a component-based application structure.
 * This makes it easier to write an app in a way that's similar to using Web Components or using Angular 2's style of application architecture.
 * 
 * Advantages of Components: 
 * simpler configuration than plain directives
 * promote sane defaults and best practices
 * optimized for component-based architecture
 * writing component directives will make it easier to upgrade to Angular 2
 */

/* ********** Component for changing personal data ********** */
demoApp.component("personal", {
    templateUrl: "personal.component.html",
    controller: ["DataStorage", "$scope",
        function (dataStorage, $scope) {
            var $ctrl = this;

            $ctrl.firstName = dataStorage.firstName;
            $ctrl.lastName = dataStorage.lastName;

            $ctrl.hasChanges = function () {
                return $ctrl.lastName !== dataStorage.lastName || $ctrl.firstName !== dataStorage.firstName;
            }

            dataStorage.preventRouteChangeIfDataChanged($scope, $ctrl.hasChanges);

            this.save = function () {
                dataStorage.checkSecurityCode().then(function () {
                    dataStorage.firstName = $ctrl.firstName;
                    dataStorage.lastName = $ctrl.lastName;
                });
            };

            this.rollback = function () {
                $ctrl.firstName = dataStorage.firstName;
                $ctrl.lastName = dataStorage.lastName;
            };
        }]
});

/* ********** Component for changing email ********** */
demoApp.component("email", {
    templateUrl: "email.component.html",
    controller: ["DataStorage", "$scope", function (dataStorage, $scope) {
        var $ctrl = this;

        $ctrl.email = dataStorage.email;

        $ctrl.hasChanges = function () { return $ctrl.email !== dataStorage.email; }

        dataStorage.preventRouteChangeIfDataChanged($scope, $ctrl.hasChanges);

        $ctrl.save = function () {
            dataStorage.checkSecurityCode()
                .then(function (res) { dataStorage.email = $ctrl.email; });
        };

        $ctrl.rollback = function () {
            $ctrl.email = dataStorage.email;
        };
    }]
});

/* ********** Component for changing phone ********** */
demoApp.component("phone", {
    templateUrl: "phone.component.html",
    controller: ["DataStorage", "$scope", function (dataStorage, $scope) {
        var $ctrl = this;

        $ctrl.phone = dataStorage.phone;

        $ctrl.hasChanges = function () { return $ctrl.phone !== dataStorage.phone; }

        dataStorage.preventRouteChangeIfDataChanged($scope, $ctrl.hasChanges);

        $ctrl.save = function () {
            dataStorage.checkSecurityCode()
                .then(function (res) { dataStorage.phone = $ctrl.phone; });
        };

        $ctrl.rollback = function () { this.phone = dataStorage.phone; };
    }]
});
/////////////////////////// E N D   C O M P O N E N T S ///////////////////////////