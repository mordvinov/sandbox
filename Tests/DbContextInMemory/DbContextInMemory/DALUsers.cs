﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DbContextInMemory.Models;

namespace DbContextInMemory
{
    public class DALUsers
    {
        private readonly IRealContext context;

        public DALUsers(IRealContext context)
        {
            this.context = context;
        }

        public List<User> GetUsersWithAName()
        {
            return context.Users.Where(x => x.Name.StartsWith("A")).ToList();
        }
    }
}
