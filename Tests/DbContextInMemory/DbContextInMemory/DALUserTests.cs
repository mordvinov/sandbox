﻿using System;
using NUnit.Framework;
using DbContextInMemory.Models;

namespace DbContextInMemory
{
    [TestFixture]
    public class DALUserTests
    {
        [Test]
        public void GetUsersWithAName_returns_only_users_with_A_name()
        {
            var context = new FakeContext();
            context.Users.AddRange(new[] {
                new User { Id = 1, Name = "Andrey" },
                new User { Id = 1, Name = "Alex" },
                new User { Id = 1, Name = "Dmitry" }
            });
            var dal = new DALUsers(context);

            var users = dal.GetUsersWithAName();

            Assert.AreEqual(2, users.Count);
        }
    }
}
