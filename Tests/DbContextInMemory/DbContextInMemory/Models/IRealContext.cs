﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DbContextInMemory.Models
{
    public interface IRealContext
    {
        DbSet<User> Users { get; set; }
        DbSet<Session> Sessions { get; set; }
    }
}
