﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DbContextInMemory.Models;
using DbContextInMemory.Utils;

namespace DbContextInMemory
{
    public class FakeContext : IRealContext
    {
        public FakeContext()
        {
            Users = new FakeDbSet<User>();
            Sessions = new FakeDbSet<Session>();
        }
        
        public DbSet<User> Users { get; set; }
        public DbSet<Session> Sessions { get; set; }
    }
}
